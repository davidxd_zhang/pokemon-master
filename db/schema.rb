# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_20_023008) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "pokemons", force: :cascade do |t|
    t.integer "poke_index"
    t.string "name", null: false
    t.string "type_1"
    t.string "type_2"
    t.integer "total"
    t.integer "hp", default: 1, null: false
    t.integer "attack", default: 1, null: false
    t.integer "defense", default: 1, null: false
    t.integer "speed_attack", default: 1, null: false
    t.integer "speed_defense", default: 1, null: false
    t.integer "speed", default: 1, null: false
    t.integer "generation", default: 1, null: false
    t.boolean "legendary", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
